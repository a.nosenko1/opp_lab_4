#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>

#define Nx 320
#define Ny 320
#define Nz 320

#define x0 (-1.0)
#define y0 (-1.0)
#define z0 (-1.0)

#define Dx 2.0
#define Dy 2.0
#define Dz 2.0

#define a 10e4
#define e 10e-8

double f(double x, double y, double z)
{
    return x * x + y * y + z * z;
}

double r(double x, double y, double z)
{
    return 6 - a * f(x, y, z);
}

void printMatrix(int size, double ***nodes)
{
    for (int z = 0; z < Nz / size; ++z)
    {
        for (int y = 0; y < Ny; ++y)
        {
            for (int x = 0; x < Nx; ++x)
            {
                printf("%2.2lf ", nodes[0][z][x + y * Nx]);
            }
            printf("\n");
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Request requests[4];
    double hx = Dx / (Nx - 1);
    double hy = Dy / (Ny - 1);
    double hz = Dz / (Nz - 1);
    double sqrHx = hx * hx;
    double sqrHy = hy * hy;
    double sqrHz = hz * hz;
    double divider = 2 / sqrHx + 2 / sqrHy + 2 / sqrHz + a;
    double localMax, max;
    double dif;
    int blockHeight = Nz / size;
    int flip = 1;
    int iter = 0;

    double ***R = (double***)malloc(blockHeight * sizeof(double**));
    double *lower = (double*)calloc(Nx * Ny, sizeof(double));
    double *upper = (double*)calloc(Nx * Ny, sizeof(double));
    double **nodes[2];
    nodes[0] = (double**)malloc(blockHeight * sizeof(double*));
    nodes[1] = (double**)malloc(blockHeight * sizeof(double*));
    for(int i = 0; i < blockHeight; ++i)
    {
        nodes[0][i] = (double*)calloc(Ny * Nx, sizeof(double));
        nodes[1][i] = (double*)calloc(Ny * Nx, sizeof(double));
        R[i] = (double**)calloc(Ny, sizeof(double*));
        for (int j = 0; j < Ny; ++j)
        {
            R[i][j] = (double*)calloc(Ny, sizeof(double));
        }
    }

    for (int z = 0; z < blockHeight; ++z)
    {
        for (int y = 0; y < Ny; ++y)
        {
            for (int x = 0; x < Nx; ++x)
            {
                R[z][y][x] = r(x0 + x * hx, y0 + y * hy, z0 + (z + rank * blockHeight) * hz);
                if (x == 0 || x == Nx - 1 || y == 0 || y == Ny - 1 || (rank == 0 && z == 0) || (rank == (size - 1) && z == (blockHeight - 1)))
                {
                    nodes[0][z][x + y * Nx] = nodes[1][z][x + y * Nx] = f(x0 + x * hx, y0 + y * hy, z0 + (z + rank * blockHeight) * hz);
                }
            }
        }
    }

    double start = MPI_Wtime();
    do
    {
        localMax = 0;
        if (rank != size - 1)    // lower send | upper recv
        {
            MPI_Isend(nodes[flip][blockHeight - 1], Nx * Ny, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &requests[0]);
            MPI_Irecv(upper, Nx * Ny, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &requests[1]);
        }
        if (rank != 0)      // lower recv | upper send
        {
            MPI_Isend(nodes[flip][0], Nx * Ny, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &requests[2]);
            MPI_Irecv(lower, Nx * Ny, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &requests[3]);
        }
        flip = 1 - flip;

        // middle calculating
        for (int z = 1; z < blockHeight - 1; ++z)
        {
            for (int y = 1; y < Ny - 1; ++y)
            {
                for (int x = 1; x < Nx - 1; ++x)
                {
                    nodes[1 - flip][z][x + y * Nx] = ((nodes[flip][z][(x + 1) + y * Nx] + nodes[flip][z][(x - 1) + y * Nx]) / sqrHx +
                                                      (nodes[flip][z][x + (y + 1) * Nx] + nodes[flip][z][x + (y - 1) * Nx]) / sqrHy +
                                                      (nodes[flip][z + 1][x + y * Nx] + nodes[flip][z - 1][x + y * Nx]) / sqrHz - R[z][y][x]) / divider;
                    dif = fabs(nodes[0][z][x + y * Nx] - nodes[1][z][x + y * Nx]);
                    if (dif > localMax)
                        localMax = dif;
                }
            }
        }

        if (rank != size - 1)
        {
            MPI_Wait(&requests[0], MPI_STATUS_IGNORE);
            MPI_Wait(&requests[1], MPI_STATUS_IGNORE);
        }
        if (rank != 0)
        {
            MPI_Wait(&requests[2], MPI_STATUS_IGNORE);
            MPI_Wait(&requests[3], MPI_STATUS_IGNORE);
        }

        // border calculating
        for (int y = 1; y < Ny - 1; ++y)
        {
            for (int x = 1; x < Nx - 1; ++x)
            {
                if (rank != 0)
                {
                    nodes[1 - flip][0][x + y * Nx] = ((nodes[flip][0][(x + 1) + y * Nx] + nodes[flip][0][(x - 1) + y * Nx]) / sqrHx +
                                                      (nodes[flip][0][x + (y + 1) * Nx] + nodes[flip][0][x + (y - 1) * Nx]) / sqrHy +
                                                      (lower[x + y * Nx] + nodes[flip][1][x + y * Nx]) / sqrHz - R[0][y][x]) / divider;
                    dif = fabs(nodes[0][0][x + y * Nx] - nodes[1][0][x + y * Nx]);
                    if (dif > localMax)
                        localMax = dif;
                }
                if (rank != size - 1)
                {
                    nodes[1 - flip][blockHeight - 1][x + y * Nx] = ((nodes[flip][blockHeight - 1][(x + 1) + y * Nx] + nodes[flip][blockHeight - 1][(x - 1) + y * Nx]) / sqrHx +
                                                                  (nodes[flip][blockHeight - 1][x + (y + 1) * Nx] + nodes[flip][blockHeight - 1][x + (y - 1) * Nx]) / sqrHy +
                                                                  (upper[x + y * Nx] + nodes[flip][blockHeight - 2][x + y * Nx]) / sqrHz - R[blockHeight - 1][y][x]) / divider;
                    dif = fabs(nodes[0][blockHeight - 1][x + y * Nx] - nodes[1][blockHeight - 1][x + y * Nx]);
                    if (dif > localMax)
                        localMax = dif;
                }
            }
        }
        MPI_Allreduce(&localMax, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        iter++;
    } while(max >= e);

    double end = MPI_Wtime();

    printf("%d Wtime: %lf\n", rank, end - start);
    if(rank == 0)
    {
        printf("iter: %d\n", iter);
    }

    localMax = 0;
    for (int z = 0; z < blockHeight; ++z)
    {
        for (int y = 1; y < Ny - 1; ++y)
        {
            for (int x = 1; x < Nx - 1; ++x)
            {
                dif = fabs(nodes[0][z][x + y * Nx] - f(x0 + x * hx, y0 + y * hy, z0 + (z + rank * blockHeight) * hz));
                if (dif > localMax)
                    localMax = dif;
            }
        }
    }
    MPI_Allreduce(&localMax, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    if (rank == 0)
        printf("max delta: %lf\n", max);

//    for (int i = 0; i < size; ++i)
//    {
//        if (rank == i)
//        {
//            printf("rank %d:\n", rank);
//            printMatrix(size, nodes);
//        }
//        MPI_Barrier(MPI_COMM_WORLD);
//   }

    MPI_Finalize();
    return 0;
}

