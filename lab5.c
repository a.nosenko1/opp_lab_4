#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <mpi/mpi.h>

#define ITERATIONS 7
#define NUM_TASKS 100
#define DEV_G 2
#define REM_TASKS_NUM 10

int tasksWeights[NUM_TASKS];

pthread_mutex_t mutex1;
pthread_mutex_t mutex2;

int remainingTasksNum;
int finishedTasksNum;

int size;   // processes number
int rank;   // current process rank

double returnResult = 0;    // будем суммировать результаты всех тасков

/* какая-нибудь задача, требующая времени */
double simpleTask();
/* инициализация весов задач, то есть сколько раз будет выполнен simpleTask пока задача выполняется */
void createTasksWeights(int *tasksWeights, int numTasks, int iterCounter);
/* выполнить полученные(имеющиеся) задачи */
void doRemainingTasks();
/* функция для потоков - исполнителей, запрашиваем новые таски и выполняем */
void* executeTasks();
/* функция для потока, раздающего задачи */
void *shareTasks();
/* создает потоки */
int createThreads(pthread_t *shareThread, pthread_t *executor);

int main(int argc, char **argv) {
    /* вызовы MPI могут делаться одновременно из нескольких потоков одного процесса */
    int provided; // if NULL we have segmentation error
    MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    double allTheTimeStart = MPI_Wtime();

    pthread_t shareTasksThread;
    pthread_t executorThread;
    if (createThreads(&shareTasksThread, &executorThread) != 0) {
        fprintf(stderr ,"Threads creating error\n");
        MPI_Finalize();
        return -1;
    }
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);

    if (rank == 0) {
        printf("all wasted time: %f\n", MPI_Wtime() - allTheTimeStart);
    }

    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    MPI_Finalize();
    return 0;
}

void createTasksWeights(int *tasksWeights, int numTasks, int iterCounter) {
    for (int i = 0; i < numTasks; i++) {
        /* вычисляем вес задачи */
        tasksWeights[i] = 5000 * abs(50 - i % NUM_TASKS) * abs(rank - (iterCounter % size));
    }
}

void* executeTasks() {
    for (int i = 0; i < ITERATIONS; ++i) {

        /* для вычисления времени выполнения тасков этим процессов в рамках одной итерации */
        double curIterStartTime = MPI_Wtime();
        
        /* задаем веса задач */
        createTasksWeights(tasksWeights, NUM_TASKS, i);
        
        /* счетчики для исполнения */
        finishedTasksNum = 0;
        remainingTasksNum = NUM_TASKS;

        /* выполняем имеющиеся таски */
        doRemainingTasks();
        
        /* запрашиваем новые таски и выполняем, если есть */
        for (int k = 0; k < size; ++k) {
            if (k != rank) {
                int numNewTasks;
                MPI_Send(&rank, 1, MPI_INT, k, 0, MPI_COMM_WORLD);
                MPI_Recv(&numNewTasks, 1, MPI_INT, k, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                if (numNewTasks > 0) {
                    /* ура, нам пришли новые таски по тэгу 1 */
                    MPI_Recv(tasksWeights, numNewTasks, MPI_INT, k, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                    remainingTasksNum = numNewTasks;
                    doRemainingTasks();
                }
            }
        }

        /* сколько времени было потрачено на текущей итерации */
        double iterationTime = MPI_Wtime() - curIterStartTime;

        /* считаем максимальное и минимальное время для посика дельта - разности */
        double minIterationTime, maxIterationTime;
        MPI_Allreduce(&iterationTime, &minIterationTime, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
        MPI_Allreduce(&iterationTime, &maxIterationTime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);

        for (int j = 0; j < size; ++j) {
            /* барьер для вывода */
            MPI_Barrier(MPI_COMM_WORLD);
            if (rank == j) {
                printf("Proc num %d: Finished tasksWeights: %d\n", rank, finishedTasksNum);
                printf("Proc num %d: Iteration time %f\n", rank, iterationTime);
            }
            if (rank == 0 && j == 0) {
                printf("\nIteration number %d\n", i);
                double delta = maxIterationTime - minIterationTime;
                printf("Delta = %f%%\n", (delta / maxIterationTime) * 100);
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
    /* отправляем сигнал, что этот поток можно закрыть, он все выполнил */
    /* по тэгу 0 */
    int stop = -1;
    MPI_Send(&stop, 1, MPI_INT, rank, 0, MPI_COMM_WORLD);

    return NULL;
}

void doRemainingTasks() {
    pthread_mutex_lock(&mutex2);
    int currentRemainingTasks = remainingTasksNum;
    pthread_mutex_unlock(&mutex2);

    for (int j = 0; j < currentRemainingTasks; j++) {
        pthread_mutex_lock(&mutex1);
        int curTaskIterations = tasksWeights[j];
        pthread_mutex_unlock(&mutex1);

        for (int m = 0; m < curTaskIterations; m++) {
            returnResult += simpleTask();
        }

        finishedTasksNum++;
        pthread_mutex_lock(&mutex2);
        currentRemainingTasks = remainingTasksNum;
        pthread_mutex_unlock(&mutex2);
    }

    pthread_mutex_lock(&mutex2);
    remainingTasksNum = 0;
    pthread_mutex_unlock(&mutex2);
}

void *shareTasks() {
    /* висим в бесконечном цикле, чекаем реквесты, если есть задачи, то отдаем */
    for (;;) {
        int request;
        MPI_Status recv_status;
        MPI_Recv(&request, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &recv_status);
        if (request == -1) {
            /* по тэгу 0 пришел сигнал, что поток все выполнил, закрываем */
            pthread_exit(NULL);
        }
        pthread_mutex_lock(&mutex1);
        if (remainingTasksNum > REM_TASKS_NUM) {
            int tasksToSendNum = remainingTasksNum / DEV_G;
            pthread_mutex_lock(&mutex2);
            remainingTasksNum -= tasksToSendNum;
            pthread_mutex_unlock(&mutex2);
            /* отправляем тому, кто прислал запрос на таски, новые таски, по тэгу 1 */
            MPI_Send(&tasksToSendNum, 1, MPI_INT, recv_status.MPI_SOURCE, 1, MPI_COMM_WORLD);
            MPI_Send(&tasksWeights[tasksToSendNum - 1], tasksToSendNum, MPI_INT, recv_status.MPI_SOURCE, 1, MPI_COMM_WORLD);
        } else {
            int tasksToSendNum = 0;
            MPI_Send(&tasksToSendNum, 1, MPI_INT, recv_status.MPI_SOURCE, 1, MPI_COMM_WORLD);
        }
        pthread_mutex_unlock(&mutex1);
    }
}

double simpleTask(){
    /* pow довольно медленная функция */
    return pow(rand(),rand());
}

int createThreads(pthread_t *shareThread, pthread_t *executor) {
    // атрибуты для executorThread
    pthread_attr_t exec_attr;
    if (pthread_attr_init(&exec_attr) != 0) {
        fprintf(stderr ,"Init attributes error");
        return -1;
    }
    // атрибуты для shareThread(раздающий таски)
    pthread_attr_t share_attr;
    if (pthread_attr_init(&share_attr) != 0) {
        pthread_attr_destroy(&exec_attr);
        fprintf(stderr ,"Init share error");
        return -1;
    }

    pthread_attr_setdetachstate(&share_attr, PTHREAD_CREATE_JOINABLE);
    pthread_attr_setdetachstate(&exec_attr, PTHREAD_CREATE_JOINABLE);

    pthread_create(shareThread, &share_attr, executeTasks, NULL);
    pthread_attr_destroy(&share_attr);

    pthread_create(executor, &exec_attr, shareTasks, NULL);
    pthread_attr_destroy(&exec_attr);

    pthread_join(*shareThread, NULL);
    pthread_join(*executor, NULL);

    return 0;
}

